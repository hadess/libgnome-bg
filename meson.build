project('libgnome-bg', 'c',
  meson_version: '>= 0.38.0',
  default_options: ['static=true']
)

assert(meson.is_subproject(), 'This project is only intended to be used as a subproject!')

gnome = import('gnome')

pkglibdir = get_option('pkglibdir')
pkgdatadir = get_option('pkgdatadir')

cdata = configuration_data()
cdata.set_quoted('GETTEXT_PACKAGE', get_option('package_name'))
cdata.set_quoted('PACKAGE_VERSION', get_option('package_version'))

libgnome_bg_gir_headers = [
  'gnome-bg-crossfade.h',
  'gnome-bg.h',
  'gnome-bg-slide-show.h',
]

libgnome_bg_gir_sources = [
  'gnome-bg.c',
  'gnome-bg-crossfade.c',
  'gnome-bg-slide-show.c',
]

libgnome_bg_deps = [
  dependency('gtk-3.0'),
  dependency('gsettings-desktop-schemas')
]

enable_static = get_option('static')
enable_introspection = get_option('introspection')

assert(not enable_static or not enable_introspection, 'Currently meson requires a shared library for building girs.')
assert(enable_static or pkglibdir != '', 'Installing shared library, but pkglibdir is unset!')

c_args = ['-DG_LOG_DOMAIN="GnomeBG"']

if enable_introspection
  c_args += '-DWITH_INTROSPECTION'
endif

if enable_static
  libgnome_bg_static = static_library('gnome-bg',
    sources: libgnome_bg_gir_sources,
    dependencies: libgnome_bg_deps,
    c_args: c_args
  )

  libgnome_bg = libgnome_bg_static
else
  if pkglibdir == ''
    error('Installing shared library, but pkglibdir is unset!')
  endif

  libgnome_bg_shared = shared_library('gnome-bg',
    sources: libgnome_bg_gir_sources + libgnome_bg_no_gir_sources,
    dependencies: libgnome_bg_deps,
    c_args: c_args,
    install_dir: pkglibdir,
    install: true
  )

  libgnome_bg = libgnome_bg_shared
endif

if enable_introspection
  assert(pkgdatadir != '', 'Installing introspection, but pkgdatadir is unset!')

  libgnome_bg_gir = gnome.generate_gir(libgnome_bg,
    sources: libgnome_bg_gir_sources + libgnome_bg_gir_headers,
    nsversion: '1.0',
    namespace: 'GnomeBG',
    includes: ['Gtk-3.0'],
    extra_args: ['-DWITH_INTROSPECTION', '--quiet'],
    install_dir_gir: pkgdatadir,
    install_dir_typelib: pkglibdir,
    install: true
  )
endif

libgnome_bg_dep = declare_dependency(
  link_with: libgnome_bg,
  include_directories: include_directories('.'),
  dependencies: libgnome_bg_deps
)

configure_file(
  output: 'config.h',
  configuration: cdata
)
