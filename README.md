# libgnome-bg

libgnome-bg is a copy library that's supposed to be used as a git sub-module.

## Projects using libgnome-bg

- [gnome-control-center](https://gitlab.gnome.org/GNOME/gnome-control-center)
- [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk/)
